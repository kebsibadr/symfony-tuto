# Liste des commandes
Toutes les commandes s'exécutent dans le dossier du projet et depuis un terminal
``` console
cd mon_projet/
```
## Symfony :
## Lancer le serveur de dev :
``` console
# serveur de base
php -S 127.0.0.1:8000 -t public
# serveur avancé
php bin/console server:run
# server Symfony
symfony serve
```
### Créer un Controller  
``` console
php bin/console make:controller DefaultController
```

### Liste de routes disponibles :
``` console
php bin/console debug:router
```

## Doctrine : 
[https://symfony.com/doc/current/doctrine.html]()
### Générer sa base de donnée
``` console
php bin/console doctrine:database:create
```
### Générer une entité
``` console
php bin/console make:entity EntityName
```
### Mettre à jour les tables
``` console
# Pour voir la requête qui va être executé
php bin/console doctrine:schema:update --dump-sql
# Pour lancer la mise à jour des table
php bin/console doctrine:schema:update --force
```
### Générer les getter et les setter
``` console
# N'existe plus dans Symfony 4
php bin/console doctrine:generate:entities /Namespace/To/Entity
```
Cette commande n'existe plus dans Symfony 4, néanmois, certains IDE proposent des plugins :  
- Pour __VSCode__ : phproberto.vscode-php-getters-setters

### Faire une requête SQL
``` console
php bin/console doctrine:query:sql 'SELECT * FROM product'
```

### Créer un système utilisateur simple
``` console
php bin/console make:user 
php bin/console make:auth
php bin/console make:registration-form
``` 

### Créer un système CRUD
``` console
php bin/console make:crud 
``` 
