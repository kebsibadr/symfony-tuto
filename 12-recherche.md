# Rechercher des annonces
> Les documentations seront tes meilleures amies si tu souhaite progresser. Il faut essayer de les comprendre et ne pas en avoir peur !
> Je t'invite donc pas à prendre à chaque fois un moment pour lire les liens qui sont proposés dans le cours.
> (à commencer par [RTFM](https://fr.wikipedia.org/wiki/RTFM_%28expression%29), qui est une expression que tu entendra sûrement un jour si tu ne lis pas les documentations).  

Le but ici est de créer un formulaire permettant de rechercher des annonces selon différents critères :
- titre ;
- prix maximum ;
- status ;
- etc...  

Nous avons plusieurs solutions pour créer le formulaire : 
- créer un formulaire en HTML ;
- profiter du système de formulaire de Symfony.

Je te propose de profiter du système de formulaire de Symfony. 
Pour cela, il est possible de créer une entité qui ne sera pas reliée à la base de données mais qui te permettra tout de même de créer un formulaire en se basant sur les propriétés de cette entité. 

## Création du formulaire
Tu peux ajouter le fichier __src/Entity/AnnonceSearch.php__ :
``` php
<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class AnnonceSearch
{

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var int|null
     */
    private $status;

    /**
     * @var int|null
     */
    private $maxPrice;
    
    /**
     * @var \DateTimeInterface|null
     */
    private $createdAt;
   
    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title = null): self
    {
        $this->title = $title;

        return $this;
    }

    public function getMaxPrice(): ?int
    {
        return $this->maxPrice;
    }

    public function setMaxPrice(?int $maxPrice = null)
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status = null): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt = null): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
```

Depuis la version de 5 de Symfony, il n'est plus possible de générer un formulaire d'une entité non reliée à la base de données avec ```php bin/console make:form```...  
Ce n'est pas grave, il est toujours possible de le créer à la main :
``` php
<?php
#src/Form/AnnonceSearchType.php

namespace App\Form;

use App\Entity\AnnonceSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use App\Entity\Annonce;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AnnonceSearchType extends AbstractType
{
    public $routeGenerator;

    public function __construct(UrlGeneratorInterface $routeGenerator)
    {
        $this->routeGenerator = $routeGenerator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Titre'
                ]
                
            ])
            ->add('maxPrice', IntegerType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Prix maximum'
                ]
            ])
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'Très mauvais' => Annonce::STATUS_VERY_BAD,
                    'Mauvais'      => Annonce::STATUS_BAD,
                    'Bon' => Annonce::STATUS_GOOD,
                    'Très bon' => Annonce::STATUS_VERY_GOOD,
                    'Parfait' => Annonce::STATUS_PERFECT
                ],
                'label' => false,
                'required' => false,
                'placeholder' => 'État',
            ])
            ->add('createdAt', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Créé après le',
                'required' => false,
            ]) 
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AnnonceSearch::class,
            'method' => 'get', // lors de la soumission du formulaire, les paramètres transiteront dans l'url. Utile pour partager la recherche par exemple
            'csrf_protection' => false,
            'action' => $this->routeGenerator->generate('app_annonce_search'), // on définit l'action qui doit traiter le formulaire. Si cette option n'est pas renseignée, le formulaire sera traité par la page en cours, ce qui n'est pas ce que l'on souhaite (tu peux essayer d'enlever cette option et envoyer le formulaire pour voir)
        ]);
    }

    public function getBlockPrefix()
    {
        // permet d'enlever les préfixe dans l'url. Tu peux commenter cette fonction, soumettre le formulaire et regarder l'url pour voir la différence.
        return '';
    }
}
```
## Affichage du formulaire
Tu peux maintenant afficher ce formulaire de façon classique, comme nous l'avons fait jusqu'à maintenant :
``` php
# dans un contrôleur
# ceci est un exemple
 /**
  * @Route("/une-route")
  */
public function recherche(Request $request): Response
{
    $search = new AnnonceSearch();
    $form = $this->createForm(AnnonceSearchType::class, $search);
    $form->handleRequest($request);
    //...

    return $this->render('template.html.twig', [
        'search' => $form->createView(),
        //...
    ]);
```
Mais il ne sera affiché que sur la route __/une-route__... 
L'idéal serait d'afficher ce formulaire sur toutes les pages, dans le header du site par exemple. 

Mais comment afficher ce formulaire sur toute les pages ? Avec le système d'[intégration du résultat de l'exécution d'un contrôleur](https://symfony.com/doc/current/templates.html#embedding-controllers).
Je te laisse lire la [documentation](https://symfony.com/doc/current/templates.html#embedding-controllers) pour en comprendre les tenants et les aboutissants.
> Sache simplement que cette façon de procéder nécessite de faire des requêtes aux contrôleurs appelés et de rendre certains templates en conséquence. 
> Cela peut avoir un impact significatif sur les performances de l'application si tu intégres de nombreux contrôleurs. 
> Pour remédier à cela, il est possible de mettre en cache les _"fragments"_ de templates avec [Edge Side Includes](https://symfony.com/doc/current/http_cache/esi.html).

### Render esi
La fonction Twig ```render_esi``` permet d'afficher le retour d'une méthode de contrôleur dans un template twig. 

Pour activer cette fonctionnalité, il faut dé-commenter une petite ligne dans __config/packages/framework.yaml__ :
``` yaml
framework:
    #...
    esi: true
    #fragments: true

```
On pourrait maintenant imaginer une méthode qui s'occupe d'afficher le formulaire et procède à la recherche lors de la soumission de ce dernier. 

#### Exercice
Je te laisse essayer d'afficher le formulaire de recherche dans le menu, grâce à ```render_esi```.
Pour cela il faut :
- créer la méthode __search__ dans __src/Controller/AnnonceController.php__ ;
- cette méthode doit retourner le formulaire __AnnonceSearchType__ (tu sais, grâce à ```$this->createForm(...)```) ;
- dans le template du menu, appeler la fonction ```render_esi``` avec la méthode du contrôleur.

---
---
---
![later](https://i.ytimg.com/vi/mVKN3Lrir2o/maxresdefault.jpg)

--- 
---
---

##### Correction

Tu peux donc créer la méthode __search__ dans __src/Controller/AnnonceController.php__ :
``` php
#src/Controller/AnnonceController.php
/**
 * @Route("/annonce/search")
 */
public function search(Request $request)
{
    $annonceSearch = new AnnonceSearch();
    $form = $this->createForm(AnnonceSearchType::class, $annonceSearch);
    
    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        // on fera le traitement plus tard
    }

    return $this->render('annonce/_search-form.html.twig', [
        'form' => $form->createView()
    ]);
}

```

Tu peux donc aussi créer un template __templates/annonce/_search-form.html.twig__ qui contiendra simplement le code suivant :
``` twig
{{ form(form) }}
```

> En allant sur __/annonce/search__, tu devrait voir le formulaire s'afficher.

Il reste maintenant à afficher le retour de la méthode __search__ (le formulaire) dans un template twig. 
Par exemple dans __templates/\_inc/nav.html.twig__ :
``` twig
<nav>
    <!-- reste du code -->
	<button class="btn btn-primary ml-1" type="button" data-toggle="collapse" data-target="#search" aria-expanded="false" aria-controls="collapseExample">
		Recherche avancée
	</button>

	<div class="collapse" id="search">
		<div class="card card-body">
			{{ render_esi(controller('App\\Controller\\AnnonceController::search')) }}
		</div>
	</div>
    <!-- reste du code -->
</nav>
```

Si tu vas voir sur n'importe quelle page, tu vois que le formulaire de recherche s'affiche toujours dans le menu grâce à ```render_esi```.

## Traitement du formulaire
Voici comment je te propose de traiter le formulaire lors de sa soumission : 
- nous allons créer une nouvelle méthode __search__ dans __src/Repository/AnnonceRepository.php__ qui ira chercher en base de données (et oui, on ne va pas sortir les annonces de notre... CHAPEAU !) ;
- nous allons appeler cette méthode __search__ quand le formulaire est envoyé, avec en paramètres, la variable __$annonceSearch__ qui contient toute les informations du formulaire.

Voici donc à quoi pourrait ressembler la méthode __search__ dans __src/Repository/AnnonceRepository.php__ :
``` php
#src/Repository/AnnonceRepository.php
//...
use App\Entity\AnnonceSearch;
//...
public function search(AnnonceSearch $annonceSearch)
{
    $query = $this->createQueryBuilder('a');
    if ($annonceSearch->getCreatedAt() !== null) {
        $query
            ->andWhere('a.createdAt > :createdAt')
            ->setParameter(':createdAt', $annonceSearch->getCreatedAt())
        ;
    }
    //...
    // autres conditions ce qui est contenu dans $annonceSearch
    //...
    return $query->getQuery()->getResult();
}
```
Et voici comment l'appeler dans le contrôleur __src/Controller/AnnonceController.php__ :
``` php
public function search(Request $request, AnnonceRepository $annonceRepository)
{
    // reste du code
    if ($form->isSubmitted()) {
        $annonces = $annonceRepository->search($annonceSearch);
        dump($annonces);
    }
```

### Exercice
Je te laisse essayer de faire la requête dans __src/Repository/AnnonceRepository.php__, selon ce qui est envoyé dans le formulaire.

![Later](https://i.ytimg.com/vi/Uqn7V-Fjjq0/maxresdefault.jpg)

#### Correction
``` php
#src/Repository/AnnonceRepository.php
public function search(AnnonceSearch $annonceSearch)
{
    $query = $this->createQueryBuilder('a');
    if ($annonceSearch->getCreatedAt() !== null) {
        $query
            ->andWhere('a.createdAt > :createdAt')
            ->setParameter(':createdAt', $annonceSearch->getCreatedAt())
        ;
    }

    if ($annonceSearch->getTitle() !== null) {
        $query
            ->andWhere('a.title LIKE :title')
            ->setParameter('title', '%'.$annonceSearch->getTitle().'%')
        ;
    }

    if ($annonceSearch->getStatus() !== null) {
        $query
            ->andWhere('a.status = :status')
            ->setParameter('status', $annonceSearch->getStatus())
        ;
    }

    if ($annonceSearch->getMaxPrice() !== null) {
        $query
            ->andWhere('a.price < :maxPrice')
            ->setParameter('maxPrice', $annonceSearch->getMaxPrice())
        ;
    }
    
    return $query->getQuery()->getResult();
}
```

## Recherche par Tag
Dans le formulaire de recherche, pourquoi pas ajouter la possibilité de rechercher par tag.

![TAG](https://media.giphy.com/media/bqbOgHSuhy257hNBYA/giphy.gif)
### Exercice
- Ajoute un champ (un select) permettant de choisir un ou plusieurs tags (tu peux t'aider de la [documentation](https://symfony.com/doc/current/reference/forms/types/entity.html)).

---
---
---
![Later](https://i.ytimg.com/vi/TXUt7toOsMM/maxresdefault.jpg)  

---
---
---

#### Correction
Dans __src/Form/AnnonceSearchType.php__ :
``` php
// n'oublions pas ces use
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Tag;
// reste du code...
->add('tags', EntityType::class, [
    'required' => false,
    'label' => false,
    'class' => Tag::class,
    'choice_label' => 'name',
    'multiple' => true
])  
```
Et il ne faut pas oublier d'ajouter la propriété tags à __src/Entity/AnnonceSearch.php__ 
```php
    // la propriété
    /**
     * @var ArrayCollection
     */
    private $tags;

    // ...

    // les getters et setters
    /**
     * Get the value of tags
     *
     * @return ArrayCollection
     */
    public function getTags(): ArrayCollection
    {
        return $this->tags;
    }

    /**
     * Set the value of tags
     *
     * @param ArrayCollection $tags
     * @return self
     */
    public function setTags(ArrayCollection $tags)
    {
        $this->tags = $tags;
        return $this;
    }
```

## La requête de recherche
Pour la requête, tu vas avoir besoin de [DQL](https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/dql-doctrine-query-language.html)
Dans les exemples de la documentation, Doctrine propose des exemples dont celui-ci:
``` php
<?php
$query = $em->createQuery('SELECT u.id FROM CmsUser u WHERE :groupId MEMBER OF u.groups');
$query->setParameter('groupId', $group);
$ids = $query->getResult();
```
Celui-ci semble convenir à notre cas. Dans __src/Repository/AnnonceRepository.php__, dans la méthode __search__ tu peux ajouter ce code dans le bloc qui s'occupe de la recherche :
``` php
if ($annonceSearch->getTags()->count() > 0) {
    $cpt = 0;
    foreach ($annonceSearch->getTags() as $key => $tag) {
        $query = $query
            ->andWhere(':tag'.$cpt.' MEMBER OF a.tags')
            ->setParameter('tag'.$cpt, $tag);
        $cpt++;
    }
}
```
Nous avons besoin d'un foreach en mettant les $cpt, sinon le champ tag serait toujours écrasé par le suivant.

Notre système de filtre devrait fonctionner avec les tags !

Il ne reste plus qu'a afficher un template avec les résultats de recherche. Mais je te laisse faire, tu as toutes les compétences requises pour y arriver.

![SKILLS](https://media.giphy.com/media/9541eIHk1MNLa/giphy.gif)