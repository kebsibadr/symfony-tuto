# Doctrine ORM
> Les documentations seront tes meilleures amies si tu souhaite progresser. Il faut essayer de les comprendre et ne pas en avoir peur !
> Je t'invite donc pas à prendre à chaque fois un moment pour lire les liens qui sont proposés dans le cours.
> (à commencer par [RTFM](https://fr.wikipedia.org/wiki/RTFM_%28expression%29), qui est une expression que tu entendra sûrement un jour si tu ne lis pas les documentations).  

[Doctrine](https://www.doctrine-project.org/) est un [ORM](https://fr.wikipedia.org/wiki/Mapping_objet-relationnel) et il vient par défaut avec Symfony.

L'objectif d'un ORM (pour Object-Relation Mapper) est simple : se charger de l'enregistrement de tes données en te faisant oublier que tu as une base de données. Tu ne vas plus écrire de requêtes, ni créer de tables via phpMyAdmin ! Dans ton code PHP, tu vas faire appel à Doctrine, l'ORM par défaut de Symfony.

## Utilises des objets et non des requêtes
Tu disposes, par exemple, d'une variable ```$annonce``` contenant un objet ```Annonce```, qui représente une annonce dans notre application.  
Habituellement, pour sauvegarder cet objet, tu ferais : ```INSERT INTO table annonce VALUES ('valeur 1', 'valeur 2', ...)```.
En utilisant un ORM, tu feras ```$orm->save($annonce)```, et ce dernier s'occupera de tout.

## Tes données sont des objets
Retiens bien cela : toutes tes données doivent être sous forme d'objet. Tu ne fera donc plus ```$annonce['title']```, mais ```$annonce->getTitle()```.  
La puissance d'un ORM ne se résume pas qu'à cela, tu pourra utiliser des méthodes du type : ```$user->getAnnonce()```, ce qui déclencherait la bonne requête.

Vocabulaire : 
- un objet dont tu confies l'enregistrement à l'ORM s'appelle une __entité__ (entity en anglais) ;
- on dit également __persister__ une entité, plutôt qu'enregistrer une entité.

> Je ne veux pas te voir faire des manipulation dans phpMyAdmin ! Pas de création de base, de création de table ou que sais-je encore ! C'est Doctrine qui va s'occuper de tout ! ![don't make me destroy you](https://media.giphy.com/media/LQx7NoYAJIozS/giphy.gif)

## Configure ta base de données
La première chose à faire est de dire à Symfony comment se connecter à la base de données. Crée un fichier __.env.local__ et ajoute la ligne suivante, en changeant les informations par les identifiants de connexions à ta base de données:
```
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
```
Dans un terminal:
``` console
php bin/console doctrine:database:create
```
Ta base de données vient d'être créée par Doctrine (tu peux aller vérifier dans phpMyAdmin, mais juste vérifier !).

## Le modèle Annonce
Pour dialoguer avec la base de données, Doctrine a besoin de modèles, qu'on appelle __Entity__ dans Symfony. Pour créer une nouvelle entité, il faut taper la commande suivante :
``` console
php bin/console make:entity
```
Le console te demande le nom de l'entité et ses champs, il suffit de répondre comme ci-dessous :
``` console
 Class name of the entity to create or update (e.g. VictoriousElephant):
 > Annonce

 created: src/Entity/Annonce.php
 created: src/Repository/AnnonceRepository.php
 
 Entity generated! Now lets add some fields!
 You can always add more fields later manually or by re-running this command.

 New property name (press <return> to stop adding fields):
 > title

 Field type (enter ? to see all types) [string]:
 > string

 Field length [255]:
 > 

 Can this field be null in the database (nullable) (yes/no) [no]:
 > no

 updated: src/Entity/Annonce.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > description

 Field type (enter ? to see all types) [string]:
 > text

 Can this field be null in the database (nullable) (yes/no) [no]:
 > yes

 updated: src/Entity/Annonce.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > 


           
  Success! 
           

 Next: When you re ready, create a migration with make:migration
```
Symfony à généré deux nouveaux fichiers :
- __src/Entity/Annonce.php__ correspond à l'entité
- __src/Repository/AnnonceRepository.php__ permet de dialoguer avec la base de données. C'est dans ces fichiers Repository, que tu écrira tes requêtes à la base de données.

Tu peux remarquer que toutes les propriétés de l'entité sont privées, c'est pour éviter que l'on mette n'importe quoi dedans. Les entités ont donc besoin de __getter__ et de __setter__ pour accéder ou modifier les propriétés.

Pour créer la table correspondant à l'entité, il faut lancer la commande suivante :
``` console
php bin/console make:migration
```
Symfony te recommande d'aller voir le fichier généré. 
``` console
Next: Review the new migration "src/Migrations/Version20190220073837.php"
```
N'hésites pas à suivre ces recommandations et ouvrir ce fichier pour voir ce qu'il contient, et surtout vérifier son contenu correspond bien à ce que tu veux faire.

Si c'est OK, tu peux lancer la commande suivante:
``` console
php bin/console doctrine:migrations:migrate
```
Tu as une nouvelle table __annonce__ qui correspond à ton entité ainsi qu'une table __migration_versions__ qui correspond aux différentes migrations (il ne faut pas toucher à cette table sauf pour des cas spécifiques).

Les migrations te serviront surtout pour mettre à jour la base de données en production. Dans un environnement de développement et si les données ne sont pas importantes, tu peux lancer la commande ```php bin/console doctrine:schema:update --force```.


> Ah oui ! Toutes les commandes peuvent être raccourcies grâce aux premières lettres de chaque mot. Ainsi ```php bin/console doctrine:schema:update --force``` peut s'écrire aussi ```php bin/console d:s:u --force```

## Ajoute des propriétés
Pour ajouter des propriétés à ton entité, tu peux de nouveau passer par la console:
``` console
php bin/console make:entity Annonce
```
Et entre les réponses suivantes:
``` console
Your entity already exists! So lets add some new fields!

 New property name (press <return> to stop adding fields):
 > price

 Field type (enter ? to see all types) [string]:
 > integer

 Can this field be null in the database (nullable) (yes/no) [no]:
 > 

 updated: src/Entity/Annonce.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > 


           
  Success! 
           

 Next: When you're ready, create a migration with make:migration
```

Lance ensuite la commande suivante :
``` console
php bin/console d:s:u --force
```

### Exercices  

Ajoute les propriétés suivantes à l'entité __Annonce__ et procède à la mise à jour de la base de données:
- sold / boolean / nullable:no
- status / integer / nullable:no
- createdAt / datetime / nullable:no

## Propriété par défaut
Il faudrait que la propriété __sold__ soit à false par défaut en base de données, tu peux donc modifier ton entité comme ci dessous:
``` php
    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $sold;
```
Procède ensuite à la mise à jour de la base de données.

## Créer une annonce
L'objectif ici est de créer une annonce en passant par la programmation. On ne pas utiliser de formulaire pour le moment. On va voir comment faire un ```INSERT INTO``` mais façon Symfony/Doctine.

Pour définir le status des articles vendus, ajoute ceci à ton entité __Annonce__:
``` php
# Tout en haut de src/Entity/Annonce.php
// reste du code...
class Annonce
{
    const STATUS_VERY_BAD  = 0;
    const STATUS_BAD       = 1;
    const STATUS_GOOD      = 2;
    const STATUS_VERY_GOOD = 3;
    const STATUS_PERFECT   = 4;
```
Et pour définir la date de création lors d'un enregistrement, tu peux ajouter un constructeur comme ci-dessous:
``` php
public function __construct()
{
    $this->createdAt = new \DateTime();
}
```
Et enfin, mettre la propriété _sold_ à false:
``` php
/**
 * @ORM\Column(type="boolean", options={"default": false})
 */
private $sold = false;
```
Remplis un nouvel objet de type __Annonce__ depuis le contrôleur grâce à cette nouvelle fonction dans __AnnonceController__:

``` php
use use Doctrine\Persistence\ManagerRegistry; // ne pas oublier ce use !

//...

/**
 * @Route("/annonce/new")
 */
public function new(ManagerRegistry $doctrine) // depuis Symfony 6, nous sommes obligé d'utiliser l'autowiring
{
    $annonce = new Annonce();
    $annonce
        ->setTitle('Ma collection de canard vivant')
        ->setDescription('Vends car plus d\'utilité')
        ->setPrice(10)
        ->setStatus(Annonce::STATUS_BAD)
        ->setSold(false)
    ;
        
    // On récupère l'EntityManager 
    $em = $doctrine->getManager();
    // On « persiste » l'entité
    $em->persist($annonce);
    // On envoie tout ce qui a été persisté avant en base de données
    $em->flush();

    die ('annonce bien créée');
}
```
Va sur la page pour créer une nouvelle annonces, tu peux aller voir en base de données et dans la debug bar.

## Récupère l'annonce
Pour récupérer un objet depuis la base de données, tu as besoin du repository __AnnonceRepository__. Dans un controller, tu peux le récupérer de plusieurs façon:
- En utilisant le ManagerRegistry :
    ``` php
    use Doctrine\Persistence\ManagerRegistry; //ne pas oublier
    
    //...

    class AnnonceController extends AbstractController
    {
        public function index(ManagerRegistry $doctrine)
        {
            $repository = $doctrine->getRepository(Annonce::class);
        }
    ```
- Encore mieux, tu peux injecter direment le Repository !
    ``` php
    // use App\Repository\AnnonceRepository; ne pas oublier

    public function index(AnnonceRepository $annonceRepository)
    {
       //...
    }
    ```

- En utilisant l'autowiring dans le __construct, ce qui pemettra d'avoir accés à cette proprité partout dans le controller :
    ``` php
    use App\Repository\AnnonceRepository; //ne pas oublier
    
    //...

    class AnnonceController extends AbstractController
    {
        /**
         * @var AnnonceRepository
         */
        private $annonceRepository; 

        public function __construct(AnnonceRepository $annonceRepository)
        {
            $this->annonceRepository = $annonceRepository;
        }
    ```

Nous utiliserons l'autowiring du repository dans une méthode. Ensuite nous pourrons utiliser les méthodes du repository. Je te laisse copier le code suivant:
    ``` php
    use App\Repository\AnnonceRepository; // ne pas oublier
    // ...
    public function index(AnnonceRepository $annonceRepository)
    {
        // rechercher une annonce par ID
        $annonce = $annonceRepository->find(1);
        dump($annonce);
        // recherche toutes les annonces
        $annonce = $annonceRepository->findAll();
        dump($annonce);
        // recherche une annonce par champ
        $annonce = $annonceRepository->findOneBy(['sold' => false]);
        dump($annonce);
    ```

Si tu actualises la page, tu peux voir tes objets dans la débug bar (la petite cible en bas).

Il faudrait afficher toutes les annonces qui ne sont pas vendues, on voit que le _findOneBy_ ne retourne qu'un seul résultat... Tu peux donc utiliser le repository.  
Dans __/src/Repository/AnnonceRepository.php__, ajoute ceci:
``` php
/**
 * @return Annonce[]
 */
public function findAllNotSold(): array
{
    return $this->createQueryBuilder('a')
        ->andWhere('a.sold = false')
        ->getQuery() // permet de créer un objet utilisable pour récupérer le résultat
        ->getResult() // permet de récupérer le résultat
    ;
}
```
Et dans le __AnnonceController__ :
``` php
$annonces = $annonceRepository->findAllNotSold();
dump($annonces);
```

## Affiche les annonces
Ajoutes différents biens comme tu l'as fait avant, en rechargeant la page ```/annonce/new``` plusieurs fois (on verra une meilleure manière de faire plus tard).  
![F5](https://media.giphy.com/media/vyVxeMNGUBT7q/giphy.gif)

### Exercice
Tente d'afficher toutes les annonces depuis la méthode ```index``` de ```AnnonceController```:
- récupère toutes les annonces dans ```index``` grâce à ```$this->annonceRepository->findAllNotSold()``` ;
- envoie toutes les annonces au template ;
- fais une [boucle](https://twig.symfony.com/doc/3.x/tags/for.html) pour afficher chaque annonce dans un élément de liste HTML;
- pour afficher une propriété d'un objet en Twig, il faut écrire ```{{ object.property }}```.
---
---
---
![Later](https://static.boredpanda.com/blog/wp-content/uploads/2019/11/20-5dce8cedac70f__700.jpg)  

---
---
---

#### Correction
Modifie __AnnonceController__ :
``` php
public function index(AnnonceRepository $annonceRepository)
{
    $annonces = $annonceRepository->findAllNotSold();

    return $this->render('annonce/index.html.twig', [
        'current_menu' => 'app_annonce_index',
        'annonces' => $annonces,
    ]);
}
```
Et ajoute ceci au template __templates/annonce/index.html.twig__ :
``` twig
{% block content %}
<div class="container">
    <h1>Liste des annonces</h1>
    <ul>
        {% for annonce in annonces %}
            <li>{{ annonce.title }}</li>
        {% endfor %}
    </ul>
</div>
{% endblock %}
```
Si tu actualise la page, tu as une liste avec des annonces.

Ajoute de la [structure](https://getbootstrap.com/docs/4.3/examples/album/) :
``` twig
{% block content %}
    <div class="container">
        <h1>Liste des annonces</h1>
        <div class="row">
        {% for annonce in annonces %}
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm" height="225" width="100%">
                    <img src="https://media.giphy.com/media/Y0zTJ7VrKo9P2/giphy.gif" class="bd-placeholder-img card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">{{ annonce.title }}</h5>
                        <p class="card-text">{{ annonce.description }}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <a class="btn btn-sm btn-outline-secondary" type="button">View</a>
                                <a class="btn btn-sm btn-outline-secondary" type="button">Edit</a>
                            </div>
                            <small class="text-muted">{{ annonce.price }}€</small>
                        </div>
                    </div>
                </div>
            </div>
        {% endfor %}
        </div>
    </div>
{% endblock %}
```

### Exercices
Crée une méthode __findLatestNotSold__ dans le repository qui retournera les 3 dernières annonces.

Affiches ces annnonces sur la home grâce à twig.

Pour t'aider, tu peux commencer par faire la requête en SQL classique et aller voir sur [la documentation de Doctrine](https://www.doctrine-project.org/index.html)

---
---
---
![Later](https://i.ytimg.com/vi/9jY4d6mGAUA/maxresdefault.jpg)  

---
---
---

#### Correction
Dans __src/Repository/AnnonceRepository.php__ :
``` php
<?php

namespace App\Repository;

use App\Entity\Annonce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Annonce|null find($id, $lockMode = null, $lockVersion = null)
 * @method Annonce|null findOneBy(array $criteria, array $orderBy = null)
 * @method Annonce[]    findAll()
 * @method Annonce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Annonce::class);
    }

    /**
     * @return Annonce[]
     */
    public function findAllNotSold(): array
    {
        return $this->findNotSoldQuery()
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Annonce[]
     */
    public function findLatestNotSold(): array
    {
        return $this->findNotSoldQuery()
            ->setMaxResults(3)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return QueryBuilder
     */
    private function findNotSoldQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.sold = false')
        ;
    }

    // /**
    //  * @return Annonce[] Returns an array of Annonce objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Annonce
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
```

Dans __src/Controller/HomeController.php__ :
``` php
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AnnonceRepository;

class HomeController extends AbstractController
{

    /**
     * @Route("/")
     */
    public function index(AnnonceRepository $annonceRepository)
    {
        $annonces = $annonceRepository->findLatestNotSold();
        return $this->render('home/index.html.twig', [
            'annonces' => $annonces
        ]);
    }
}
```
Et dans __templates/home/index.html.twig__ :
``` twig
{% extends "base.html.twig" %}

{% block content %}
<div class="container">
    <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">SmallAds</h1>
        <p class="lead">Vends toutes tes affaires !</p>
    </div>
</div>

<div class="container">
    <h2>Les dernières annonces</h2>
    <div class="row">
        {% for annonce in annonces %}
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm" height="225" width="100%">
                    <img src="https://media.giphy.com/media/Y0zTJ7VrKo9P2/giphy.gif" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">{{ annonce.title }}</h5>
                        <p class="card-text">{{ annonce.description }}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <a class="btn btn-sm btn-outline-secondary" type="button">View</a>
                                <a class="btn btn-sm btn-outline-secondary" type="button">Edit</a>
                            </div>
                            <small class="text-muted">{{ annonce.price }}€</small>
                        </div>
                    </div>
                </div>
            </div>
        {% endfor %}
    </div>
</div>
{% endblock %}
```

## Détail d'une annonce

En suivant le chemin  ```/annonce/{id de l'annonce}```, il faudrait afficher le détail d'une annonce. En clair, si je tape http://127.0.0.1:8000/annonce/2, je devrait tomber sur le détail de l'annonce d'id 2 en base de données.

### Exercice
- Dans __AnnonceController__, crée une nouvelle méthode ```show``` ;
- cette méthode répond à la route ```/annonce/{id}```. Cet id __doit__ forcément être un _int_, sinon la route ne devra pas correspondre. Pour en apprendre plus sur les routes, c'est [par ici](https://symfony.com/doc/current/routing.html) ;
- dans cette méthode, tu peux chercher l'annonce avec son id grâce à son repository ;
- envoie l'annonce trouvée à la vue Twig que tu auras préalablement créée ```templates/annonce/show.html.twig``` ;
- si l'annonce n'est pas trouvée, envoie une erreur 404 (https://symfony.com/doc/current/controller.html#managing-errors-and-404-pages) ;
- affiche les détails de l'annonce dans ce template.

---
---
---
![Later](https://i.ytimg.com/vi/xYqz04A2WEw/maxresdefault.jpg)  

---
---
---

#### Correction
10 secondes ! Tu commences à être un pro !

Ajoute cette méthode au controller __src/Controller/AnnonceController.php__ :
``` php
    /**
     * @Route(
     *  "/annonces/{id}", 
     *  requirements={"id": "\d+"}
     * )
     * @return Response
     */
    public function show(int $id, AnnonceRepository $annonceRepository): Response
    {

        $annonce = $annonceRepository->find($id);

        if (!$annonce) {
            return $this->createNotFoundException();
        }
        return $this->render('annonce/show.html.twig', [
            'annonce' => $annonce,
        ]);
    }
```
Ou encore mieux, tu peux utiliser l'autowiring dès qu'il y'a le mot clé __id__ dans ta route, Symfony se chargera lui même d'instancier l'entité et de lever une 404 si cette entité n'existe pas :
``` php
    /**
     * @Route(
     *  "/annonces/{id}", 
     *  requirements={"id": "\d+"}
     * )
     * @return Response
     */
    public function show(Annonce $annonce): Response
    {
        return $this->render('annonce/show.html.twig', [
            'annonce' => $annonce,
        ]);
    }
```
Tu peux vérifier que le code de réponse de chaque page dans la débug bar en bas à gauche.

Enfin, crée le template __templates/annonce/show.html.twig__ :
``` twig
{% extends '_layout/full-width.html.twig' %}

{% block title %}#{{ annonce.id }}-{{ annonce.title }}!{% endblock %}
{% block content %}
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <img class="img-fluid" src="https://media.giphy.com/media/utp2JUuE3znt6/giphy.gif">
                </div>
                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <div class="card-header">
                            <h4 class="my-0 font-weight-normal">{{ annonce.title }}</h4>
                        </div>
                        <div class="card-body">
                            <p class="h1 card-title pricing-card-title">
                                {{ annonce.price }} €
                            </p>
                            
                            <p>
                                {% if annonce.status == constant('App\\Entity\\Annonce::STATUS_VERY_BAD') %}
                                    <span class="badge btn btn-outline-danger">État très mauvais</span>
                                {% elseif annonce.status == constant('App\\Entity\\Annonce::STATUS_BAD') %}
                                    <span class="badge btn btn-outline-warning">Mauvais état</span>
                                {% elseif annonce.status == constant('App\\Entity\\Annonce::STATUS_GOOD') %}
                                    <span class="badge btn btn-outline-primary">Bon état</span>
                                {% elseif annonce.status == constant('App\\Entity\\Annonce::STATUS_VERY_GOOD') %}
                                    <span class="badge btn btn-outline-info">Très bon état</span>
                                {% elseif annonce.status == constant('App\\Entity\\Annonce::STATUS_PERFECT') %}
                                    <span class="badge btn btn-outline-success">Parfait état</span>
                                {% endif %}
                            </p>
                            <button class="btn btn-lg btn-block btn-outline-primary" type="button">Contacter le vendeur</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-4">
            <div class="col-md-12">
                <h2>Déscription</h2>
                <p>{{ annonce.description }}</p>
            </div>
        </div>
    </div>
{% endblock %}
```

## Slug
Sur https://packagist.org/, tu peux trouver un tas de librairie qui peuvent être installées dans ton projet via Composer. N'hésites pas à chercher si une librairie existe pour répondre à ton besoin.

Ici, nous souhaitons générer des liens seo-friendly pour nos annonces. Imaginons que le titre d'une annonce soit le suivant : _"Vends Super Canard trouvé en boulangerie-pâtisserie"_. Il faudrait pouvoir accéder à cette annonce via une url du genre _/annonce/vends-super-canard-trouve-en-boulangerie-patisserie_.

Nous allons utiliser [slugify](https://packagist.org/packages/cocur/slugify) pour générer des liens corrects vers les annonces.  

Il convient d'ajouter cette librairie au projet en tapant :
``` console
composer require cocur/slugify
```

### Exercice
- Ajoute une propriété _slug_/_255_/notNull à l'entité __Annonce__ ;
- met à jour la base de données ;
- dans la création d'une annonce (_/annonce/new_), tu peux ajouter ```->setSlug('Vends Super Canard trouvé en boulangerie-pâtisserie')``` avant l'enregistrement en base de données pour ajouter un slug à l'annonce ;
- dans l'entité __Annonce__, modifies la fonction ```setSlug``` afin de transformer le paramètre ```$slug``` en slug valide (grâce à la lib Slugify) ;
- dans __AnnonceController.php__, crées une nouvelle fonction ```showBySlug``` qui répond à la route ```/annonce/{slug}-{id}``` ;
- cette fonction va chercher en base de données l'annonce selon le slug et l'id passés dans la route, et affiche le détail de l'annonce.

---
---
---
![Later](https://i.ytimg.com/vi/DH0BHW5Il-k/maxresdefault.jpg)  

---
---
---
#### Correction
``` php
# src/Entity/Annonce.php
/**
 * En haut du fichier
 */
use Cocur\Slugify\Slugify;
/**
 * Le reste du code
 */
public function getSlug(): ?string
{
    if (!$this->slug) {
        $this->setSlug($this->title);
    }
    return $this->slug;
}

public function setSlug(string $slug): self
{
    $slugify = new Slugify();
    $this->slug = $slugify->slugify($slug);

    return $this;
}
```

Et ajoutes cette méthode au controller __src/Controller/AnnonceController.php__ :
``` php
/**
 * @Route(
 *  "/annonce/{slug}-{id}", 
 *  requirements={"slug": "[a-z0-9\-]*", "id": "\d+"}
 * )
 * @return Response
 */
public function showBySlug(string $slug, int $id, AnnonceRepository $annonceRepository): Response
{
    $annonce = $annonceRepository->findOneBy([
        'slug' => $slug,
        'id' => $id
    ]);

    if (!$annonce) {
        return $this->createNotFoundException();
    }

    return $this->render('annonce/show.html.twig', [
        'annonce' => $annonce,
    ]);
}
```

Tu peux aussi utiliser l'injection de dépendance :
``` php
/**
 * @Route(
 *  "/annonces/{slug}-{id}", 
 *  requirements={"slug": "[a-z0-9\-]*", "id": "\d+"}
 * )
 * @return Response
 */
public function showBySlug(Annonce $annonce, $slug): Response
{
    //$annonce = $this->annonceRepository->find($id);

    return $this->render('annonce/show.html.twig', [
        'current_menu' => 'app_annonce_index',
        'annonce' => $annonce, // Symfony fait le find à notre place grâce à l'injection et l'id
    ]);
}
```

Dans tes templates, ajoute le lien vers les annonces :
``` twig
<a href="{{ path('app_annonce_show', {id: annonce.id, slug: annonce.slug}) }}" class="btn btn-sm btn-outline-secondary" type="button">View</a>
```

## Evénement Doctrine
Il est possible d'améliorer un peu l'enregistrement du slug et de la date de création de l'entité Annonce. 
En effet, ce que nous avons fait est correct mais Doctrine nous permet d'utiliser des événements qui se déclenchent lors du cycle de vie d'une entité : avant un persit, avant un enregistrement en base de données, après le chargement depuis la base de données, etc...

Dans certains cas, tu peux avoir besoin d'effectuer des actions juste avant ou juste après la création, la mise à jour ou la suppression d'une entité. 
Par exemple, si tu stockes la date d'édition d'une annonce, à chaque modification de l'entité Annonce il faut mettre à jour cet attribut juste avant la mise à jour dans la base de données.

Ces actions, tu dois les faire à chaque fois. 
Cet aspect systématique a deux impacts. 
D'une part, cela veut dire qu'il faut être sûrs de vraiment les effectuer à chaque fois pour que ta base de données soit cohérente. 
D'autre part, cela veut dire qu'on est bien trop fainéants pour se répéter !

C'est ici qu'interviennent les évènements Doctrine. 
Plus précisément, les callbacks du cycle de vie (lifecycle en anglais) d'une entité. 
Un callback est une méthode de ton entité, et on va dire à Doctrine de l'exécuter à certains moments.

On parle d'évènements de « cycle de vie », car ce sont différents évènements que Doctrine déclenche à chaque moment de la vie d'une entité : 
son chargement depuis la base de données, sa modification, sa suppression, etc. 

Pour en savoir plus, tu peux lire la [documentation](https://symfony.com/doc/current/doctrine/events.html) et regarder cette [vidéo](https://www.youtube.com/watch?v=-HoTTylU3to). 
Cette fonctionnalité te sera surement utile plus tard, ne passe pas à côté.

Je te propose donc de modifier légérement l'entité __Annonce__ de la façon suivante :
``` php
# src/Entity/Annonce.php
// ...
/**
 * ...
 * @ORM\HasLifecycleCallbacks()
 */
class Annonce
{
    // reste du code ...
    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->slug = (new Slugify())->slugify($this->title);
    }

    // plus besoin du constructeur
    /*public function __construct()
    {
        $this->createdAt = new \DateTime();
    }*/
```
Essaye maintenant de recharger la page _annonce/new_ et vérifie que l'annonce s'est bien enregistrée avec un slug.

> Bravo si tu es arrivé jusque là sans encombre, on se retrouve après pour la suite   
![Bravo](https://media.giphy.com/media/Swx36wwSsU49HAnIhC/giphy.gif)
