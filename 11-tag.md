# Tag
> Les documentations seront tes meilleures amies si tu souhaite progresser. Il faut essayer de les comprendre et ne pas en avoir peur !
> Je t'invite donc pas à prendre à chaque fois un moment pour lire les liens qui sont proposés dans le cours.
> (à commencer par [RTFM](https://fr.wikipedia.org/wiki/RTFM_%28expression%29), qui est une expression que tu entendra sûrement un jour si tu ne lis pas les documentations).  

L'objectif ici est de pouvoir ajouter des tags aux annonces. Une annonce pourra avoir plusieurs tags et un tag pourra avoir plusieurs annonces.
Pour mettre ce système en place, nous allons avoir besoin de créer une relation entre ces deux entités.

## Rappel sur les relations :
![schema des relations](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1rrzOny4FT926FF46EAXHR2XRISHaGSuIvzlUIxSKT8Q8Cp_N "schema des relations") 
### ManyToOne n..1
Cette relation spécifie que la base est liée à une seule cible et qu’une cible peut être liée à plusieurs bases.
#### Exemple
Un Tag est lié à une seul Annonce
Une Annonce est liée à plusieurs Tags.

### OneToMany 1..n
Cette relation spécifie que la base peut être liée à plusieurs cible, mais qu’une cible est liée à une seule base.
#### Exemple
Un Tag est lié à plusieurs Annonces.
Une Annonce est liée à un seul Tag.

### ManyToMany n..n
Cette relation spécifie que la base peut être liée à plusieurs cibles et qu’une cible peut être liée à plusieurs bases.
#### Exemple
Un Tag est lié à plusieurs Annonces.
Une Annonce est liée à plusieurs Tag.

### OneToOne 1..1
La relation OneToOne n’est pas la plus utilisée.
Cette relation spécifie que la base est liée à une seule cible et qu’inversement une cible est liée à une seule base.
#### Exemple
Un Tag est lié à exactement une Annonce.
Une Annonce est liée à exactement un Tag.

## L'entité Tag
Tu l'aura compris, la relation qui correspond à notre besoin est la relation __ManyToMany__.

Créons sans plus attendre l'entité Tag::
``` console
php bin/console make:entity

Class name of the entity to create or update (e.g. TinyJellybean):
 > Tag

 created: src/Entity/Tag.php
 created: src/Repository/TagRepository.php
 
 Entity generated! Now let s add some fields!
 You can always add more fields later manually or by re-running this command.

 New property name (press <return> to stop adding fields):
 > name

 Field type (enter ? to see all types) [string]:
 > string

 Field length [255]:
 > 

 Can this field be null in the database (nullable) (yes/no) [no]:
 > no

 updated: src/Entity/Tag.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > annnonces

 Field type (enter ? to see all types) [string]:
 > relation

 What class should this entity be related to?:
 > Annonce

What type of relationship is this?
 ------------ ------------------------------------------------------------------ 
  Type         Description                                                       
 ------------ ------------------------------------------------------------------ 
  ManyToOne    Each Tag relates to (has) one Annonce.                            
               Each Annonce can relate to (can have) many Tag objects            
                                                                                 
  OneToMany    Each Tag can relate to (can have) many Annonce objects.           
               Each Annonce relates to (has) one Tag                             
                                                                                 
  ManyToMany   Each Tag can relate to (can have) many Annonce objects.           
               Each Annonce can also relate to (can also have) many Tag objects  
                                                                                 
  OneToOne     Each Tag relates to (has) exactly one Annonce.                    
               Each Annonce also relates to (has) exactly one Tag.               
 ------------ ------------------------------------------------------------------ 

 Relation type? [ManyToOne, OneToMany, ManyToMany, OneToOne]:
 > ManyToMany

 Do you want to add a new property to Annonce so that you can access/update Tag objects from it - e.g. $annnonce->getTags()? (yes/no) [yes]:
 > yes

 A new property will also be added to the Annonce class so that you can access the related Tag objects from it.

 New field name inside Annonce [tags]:
 > 

 updated: src/Entity/Tag.php
 updated: src/Entity/Annonce.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > 


           
  Success! 
           

 Next: When you're ready, create a migration with make:migration
```
N'hésite pas à aller voir ta nouvelle entité __src/Entity/Tag.php__ :
``` php
<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    // On voit la relation vers les annonces
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Annonce", inversedBy="tags")
     */
    private $annonces;

    // les annonces sont initialisées grâce à un ArrayCollection
    // qui permet d'avoir des méthodes facilitant la manipulation des tableaux
    public function __construct()
    {
        $this->annonces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Annonces[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annnonces[] = $annonce;
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->contains($annonce)) {
            $this->annnonces->removeElement($annonce);
        }

        return $this;
    }
}
```
Et va voir ton entité __Annonce__ :
``` php
<?php

class Annonce
{
    // reste du code

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", mappedBy="annonces")
     */
    private $tags;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->tags = new ArrayCollection();
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addAnnonce($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removeAnnonce($this);
        }

        return $this;
    }
}
```

N'oublie pas de procéder à la mise à jour de la base de données ! 
Je te propose de faire une migration pour cette fois, cela nous permettra de voir le processus de rollback (revenir en arrière dans la base de données).
``` console
php bin/console make:migrations
php bin/console doctrine:migration:migrate
```

## CRUD (Create Read Update Delete)
À la manière de ```php bin/console make:controller``` Symfony nous permet de générer un système de [CRUD](https://fr.wikipedia.org/wiki/CRUD) pour une entité.
Pour cela, il te suffit de taper :
``` console
php bin/console make:crud Tag
```
Automatiquement, Symfony créé le controller __src/Controller/TagController.php__, son Repository __src/Repository/TagRepository.php__, le FormType __src/Form/TagType.php__, et les templates liés :
- templates/tag/_delete_form.html.twig
- templates/tag/_form.html.twig
- templates/tag/edit.html.twig
- templates/tag/index.html.twig
- templates/tag/new.html.twig
- templates/tag/show.html.twig

Tu peux aller voir le résultat grâce au chemin spécifié dans le controller : __/tag__
En une seule ligne de commande, Symfony à généré tout le système de gestion de tag.
À nous maintenant de l'adapter à nos besoins !

Si tu essaies de créer un tag, tu devrais avoir une erreur. 
Pour la corriger, il faut éditer __src/Form/TagType.php__
``` php
public function buildForm(FormBuilderInterface $builder, array $options)
{
    $builder
        ->add('name')
        //->add('annonce') // c'est une Entité et non un type de champ. Symfony ne sait pas comment afficher cette entitié dans le formulaire html, d'où l'erreur
    ;
}
```
Essaies de créer quelques tags !

## Association des Tags
Lors de la création d'une annonce, il serait logique d'ajouter un champ dans le formulaire, permettant de choisir un tag.  

### Exercice
Pour ce faire, je te laisse essayer d'ajouter un champ de type __ChoiceType__ dans le formulaire de création d'une annonce.  
Comme toujours, [la documentation pourra t'aider](https://symfony.com/doc/current/reference/forms/types/entity.html) ;).

---
---
---
![later](https://i.ytimg.com/vi/_8KGlebWqTQ/maxresdefault.jpg)

--- 
---
---
#### Correction
Pour associer des Tags à une annonce, tu peux modifier le formulaire d'édition d'annonce __src/Form/AnnonceType.php__ : 
``` php
->add('tags', EntityType::class, [
    'class' => Tag::class,
    'choice_label' => 'name', // c'est la propriété dans l'entitié Tag qui sera affichée dans le select
    'multiple' => true
])
```
Tu peux maintenant essayer d'associer des Tags à une annonce.
Il semble que ça ne soit pas prit en compte !

### Notion de propriétaire et d'inverse
La notion de propriétaire et d'inverse est abstraite mais importante à comprendre. 
Dans une relation entre deux entités, il y a toujours une entité dite propriétaire, et une dite inverse. 
L'entité propriétaire et l'entité qui est responsable de la relation. [Voir la documentation](https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/association-mapping.html#owning-and-inverse-side-on-a-manytomany-association)

C'est donc l'entitié Annonce qui devrait être le __owning side__, et non l'entité Tag. 
En effet, on va plus souvent faire ```$annonces->getTags()``` plutôt que l'inverse.

Pour se faire, tu peux modifier __src/Entity/Annonce.php__ :
``` php
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="annonces")
     */
    private $tags;

```
et dans __src/Entity/Tag.php__ :
``` php
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Annonce", mappedBy="tags")
     */
    private $annnonces;
```
```inversedBy``` signifie : je suis l'entité propriétaire
```mappedBy``` signifie : je ne suis pas l'entité propriétaire

Si tu essaies d'éditer une annonce, tu as une erreur car la table intérmédiaire n'est plus correcte. 
Effectivement, elle devrait se nommer annonce_tag et non tag_annonce maintenant (c'est l'entité propriétaire qui est en premier).

### RollBack
Nous allons donc changer cela en base de données en procédant à un rollback vers une version antérieure à la base de données actuelle. 
Pour cela :
``` console
php bin/console doctrine:migrations:status

 == Configuration

    >> Name:                                               Application Migrations
    >> Database Driver:                                    pdo_mysql
    >> Database Host:                                      127.0.0.1
    >> Database Name:                                      samllads
    >> Configuration Source:                               manually configured
    >> Version Table Name:                                 migration_versions
    >> Version Column Name:                                version
    >> Migrations Namespace:                               DoctrineMigrations
    >> Migrations Directory:                               /home/vagrant/smallads/src/Migrations
    >> Previous Version:                                   2019-02-21 15:19:38 (20190221151938)
    >> Current Version:                                    2019-02-23 14:27:28 (20190223142728)
    >> Next Version:                                       Already at latest version
    >> Latest Version:                                     2019-02-23 14:27:28 (20190223142728)
    >> Executed Migrations:                                10
    >> Executed Unavailable Migrations:                    0
    >> Available Migrations:                               10
    >> New Migrations:                                     0
```
Ce qui nous intéresse se situe à la ligne ```>> Previous Version: 2019-02-21 15:19:38 (```__20190221151938__```)```: c'est le timestamp de la versions précédente.

Tu peux revenir à cette version en faisant :
``` console
php bin/console doctrine:migrations:migrate 20190221151938
```
On voit qu'il fait des drop table :
``` console
-- reverting 20190223142728

     -> ALTER TABLE tag_annonce DROP FOREIGN KEY FK_C464BE4FBAD26311
     -> DROP TABLE tag
     -> DROP TABLE tag_annonce
```
Tu peux donc supprimer ta dernière migration (en vérifiant bien que c'est celle qui est concerné !) et en générer une nouvelle :
``` console
php bin/console make:migration
```
puis si le fichier est correcte:
``` console
php bin/console doctrine:migrations:migrate
```
Ajoute quelques tags et essaies de créer une annonce ! Normalement tout fonctionne !

> Si le processuss de rollback cela ne fonctionne pas, ou que tu es perdu·e, tu peux faire un ```php bin/console doctrine:schema:update --force``` et supprimer la migration créée plus tôt.
> Sache simplement qu'il est possible de revenir en arrière dans la base de données grâce aux migration.

## Cosmétiques des select
Pour rendre les select un peu plus sympa tu peux utiliser [select2](https://cdnjs.com/libraries/select2).
Il suffit d'ajouter le librarie et ce code dans le template __templates/base.html.twig__: 
``` twig
{% block stylesheets %}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
{% endblock %}


{% block javascripts %}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script>$('select').select2()</script>
{% endblock %}
```

## Authorization
N'hésite pas à mettre en pratique ce que tu as vu dans les partie précédentes pour mettre en place les authorizations permettant de gérer les tags dans la partie __admin__.

## Conclusion
Tu sais maintenant créer des relations ManyToMany et un système de CRUD. Bien joué !

![bien joué](https://media.giphy.com/media/aLdiZJmmx4OVW/giphy.gif)