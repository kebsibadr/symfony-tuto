# Formulaires
> Les documentations seront tes meilleures amies si tu souhaite progresser. Il faut essayer de les comprendre et ne pas en avoir peur !
> Je t'invite donc pas à prendre à chaque fois un moment pour lire les liens qui sont proposés dans le cours.
> (à commencer par [RTFM](https://fr.wikipedia.org/wiki/RTFM_%28expression%29), qui est une expression que tu entendra sûrement un jour si tu ne lis pas les documentations).  

Dans Symfony, il est possible de créer des formulaires directement liés aux entités. Ces formulaires seront sous forme de classe php et il sera possible de générer les inputs correspondant aux propriétés de l'entité  en quelques lignes : [voir la doc](https://symfony.com/doc/current/forms.html)

![input](https://media.giphy.com/media/PuEbN8C4ttcNa/giphy.gif)
## Administration
Crée un contrôleur qui s'occupera de la partie administration des annonces :
``` console
php bin/console make:controller Admin\\Annonce
```

### Exercices
- Dans le contrôleur généré, récupère toutes les annonces depuis la base données ;
- renvoie toutes les annonces au template ;
- affiche les annonces dans le template sous forme de tableau avec au moins
    - une colonne titre ;
    - une colonne action avec un bouton __Éditer__ ;
- crée une méthode __edit__ qui répondra à la route __/admin/annonce/{id}/edit__ ;
- ajoute un lien au bouton __Éditer__ qui redirige vers cette méthode.

---
---
---
![later](https://i.ytimg.com/vi/mVKN3Lrir2o/maxresdefault.jpg)

--- 
---
---

#### Correction
Dans __src/Controller/Admin/AnnonceController.php__ :
``` php
<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AnnonceRepository;
use Symfony\Component\HttpFoundation\Response;

# note la route qui sera préfixée pour chaque route du contrôleur
/**
 * @Route("/admin")
 */
class AnnonceController extends AbstractController
{
    /**
     * @Route("/annonce")
     */
    public function index(AnnonceRepository $annonceRepository)
    {
        $annonces = $annonceRepository->findAll();
        return $this->render('admin/annonce/index.html.twig', [
            'annonces' => $annonces
        ]);
    }
    
    /**
     * @Route("/annonce/{id}/edit")
     */
    public function edit()
    {
        return new Response('Edition');
    }
}
```

Dans __templates/admin/annonce/index.html.twig__:
``` twig
{% extends '_layout/sidebar.html.twig' %}
{% block title %}Gérer les annonces!{% endblock %}
{% block content %}
    <div class="container">
        <h1>Gérer les annonces</h1>
        <div class="row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Titre</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {% for annonce in annonces %}
                        <tr>
                            <td>{{ annonce.title }}</td>
                            <td>
                                <a href="{{ path('app_admin_annonce_edit', {id: annonce.id}) }}" class="btn btn-secondary">Éditer</a>
                            </td>
                        </tr>
                    {% endfor %}
                </tbody>
            </table>
            
        </div>
    </div>
{% endblock %}
```

Pour faire fonctionner le bouton Éditer, il nous faut créer une route et sa méthode.

## Édition
Modifie la méthode __edit__ pour renvoyer un template :
``` php
/**
 * @Route("/annonce/{id}/edit")
 */
public function edit(Annonce $annonce)
{
    return $this->render('annonce/edit.html.twig', [
        'annonce' => $annonce
    ]);
}
```

Et le template __templates/annonce/edit.html.twig__ :
``` twig
{% extends '_layout/sidebar.html.twig' %}
{% block title %}Éditer l'annonce!{% endblock %}
{% block content %}
    <div class="container">
        <h1>Éditer l'annonce</h1>
    </div>
{% endblock %}
```

### Exercice  
Objectif : éditer une annonce.  
En suivant [la doc sur les formulaires](https://symfony.com/doc/current/forms.html), essaye de :
- Créer une classe de formulaire (un FormType) qui sera mapper sur l'entité __Annonce__ ;
- grâce à la fonction __edit__, affiche ce formulaire dans le template __templates/annonce/edit.html.twig__ ;
- lors de l'envoi du formulaire, toujours dans la fonction __edit__, met à jour l'annonce avec les informations envoyées dans le formulaire.
> tout est dans la DOC !
---
---
---
![later](https://i.ytimg.com/vi/U7CZcd-UYmU/maxresdefault.jpg)

--- 
---
---

#### Correction

##### Création et affichage
Pour créer le formulaire, il faut commencer par créer une classe __Type__. Les __Types__ sont des classes php dont le nom commence par le nom de l'entité pour laquelle tu veux créer un formulaire et est suffixé par __Type__ (en général, rien ne t'empêche de changer le nom selon tes besoins). 
``` console
php bin/console make:form
```
Et répondre de la manière suivante :
``` console
The name of the form class (e.g. OrangePuppyType):
> AnnonceType

The name of Entity or fully qualified model class name that the new form will be bound to (empty for none):
> Annonce

created: src/Form/AnnonceType.php


    Success! 


Next: Add fields to your form and start using it.
Find the documentation at https://symfony.com/doc/current/forms.html
```
On se retrouve avec un nouveau fichier __src/Form/AnnonceType.php__ que tu peux ouvrir.

``` php
# src/Form/AnnonceType.php
<?php

namespace App\Form;

use App\Entity\Annonce;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnnonceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('price')
            ->add('sold')
            ->add('status')
            ->add('createdAt')
            ->add('slug')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Annonce::class,
        ]);
    }
}
```

Pour afficher le formulaire, modifie __src/Controller/Admin/AnnonceController.php__ :
``` php
/**
 * @Route("/annonce/{id}/edit")
 */
public function edit(Annonce $annonce)
{
    $form = $this->createForm(AnnonceType::class, $annonce);

    return $this->render('annonce/edit.html.twig', [
        'annonce' => $annonce,
        'form' => $form->createView() // la méthode createView est très important ! Si tu l'oublie, Twig ne pourra pas interpréter le formulaire
    ]);
}
```
Et modifie __templates/annonce/edit.html.twig__ :
``` twig
{% block content %}
    <div class="container">
        <h1>Éditer l'annonce</h1>
        {{ form_start(form) }}
            {{ form_widget(form) }}
            <button class="btn btn-primary">Sauvegarder</button>
        {{ form_end(form) }}
    </div>
{% endblock %}
```

> Tu peux [changer le thème](https://symfony.com/doc/current/form/form_themes.html) des formulaire dans __config/packages/twig.yaml__ et ajouter 
``` yaml
    form_themes: ['bootstrap_4_horizontal_layout.html.twig']
```

Pour customiser le formulaire (c'est aussi dans la doc ! https://symfony.com/doc/current/form/form_customization.html), il est aussi possible de séparer les différents champs :
``` twig
<div class="col-md-4">
    {{ form_row(form.title) }}
</div>
<div class="col-md-4">
    {{ form_row(form.description) }}
</div>
<div class="col-md-4">
    {{ form_row(form.description) }}
</div>
```

##### Soumission du formulaire
Pour enregistrer un changement sur une entité, tu as besoin de l'EntityManager de Doctrine.
Tu peux soit le récupérer en injectant ManagerRegistry dans ta méthode 
```php
use Doctrine\Persistence\ManagerRegistry;
//...
    public function edit(ManagerRegistry $doctrine) 
    {
    $em = $doctrine->getManager();
    }
    
```  
Soit injecter directement EntityManagerInterface, qui instancira directement le manager.  
Dans __src/Controller/Admin/AnnonceController.php__ :
``` php
# ne pas oublier ces use
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
# ...
    /**
     * @Route("/annonce/{id}/edit", methods={"POST", "GET"})
     */
    public function edit(Annonce $annonce, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(AnnonceType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('app_admin_annonce_index');
        }
            
        return $this->render('annonce/edit.html.twig', [
            'annonce' => $annonce,
            'form' => $form->createView()
        ]);
    }
```
Essaies de soumettre le formulaire et admire le résultat !

#### WUT ??
![WUT ?](https://media.giphy.com/media/11HdhZBMpdxClG/giphy.gif)

Comment l'annonce a-t-elle pu se mettre à jour, alors qu'on ne l'hydrate pas avec les informations que l'on reçoit du formulaire ?

Et oui, en PHP classique tu as peut être l'habitude de procéder ainsi :
``` php
$annonce = new Annonce();

$annonce->setTitle($_POST['title']);
$annonce->setDescription($_POST['description']);
// etc...
```

Ce qui est important ici ce sont ces deux lignes 
``` php
$form = $this->createForm(AnnonceType::class, $annonce);
$form->handleRequest($request);
```

Grâce à ```$this->createForm(AnnonceType::class, $annonce);$this->createForm(AnnonceType::class, $annonce);```, Symfony sait que le formulaire est mappé sur l'instance de Annonce, il connaît ses propriétés. 

```$form->handleRequest($request);``` permet quant à elle d'hydrater l'objet avec les informations passé en POST.
### Exercice
Tu te souviens des événements Doctrine ???

![Remember ?](https://media.giphy.com/media/3ohjUNm1c3oMRTcyoU/giphy.gif)

Le but est d'ajouter un champ ```updatedAt``` à l'entité annonce, de manière à savoir quand est ce qu'une annonce à été éditée.
- ajoute une propriété ```updatedAt``` au modèle __Annonce__ ;
- en suivant la [documentation](https://symfony.com/doc/current/doctrine/events.html), mets à jour le champ ```updatedAt``` avant chaque mise à jour ;
- essaies d'éditer une annonce et vérifie que le champ ```updatedAt``` est bien mis à jour à chaque édition.

---
---
---
![Later](https://i.ytimg.com/vi/TxSrZFB9AT0/hqdefault.jpg)  

---
---
---
#### Correction
``` php
# src/Entity/Annonce.php


    // reste du code ...
    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        //...
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

```

## Option de formulaire
Grâce aux options de formulaire, on va pouvoir, par exemple, conditionner l'affichage des champs selon le rôle de l'utilisateur, ou encore spécifier si un champ est requis ou non, donner une valeur à un champ, etc...

N'hésite pas à lire la [doc à ce sujet](https://symfony.com/doc/current/forms.html#other-common-form-features), cela nous servira plus tard pour afficher ou non des champs selon que l'utilisateur est admin ou non.


### Les labels
Pour changer les labels du formulaire tu peux soit procéder de la manière suivante :
``` php
$builder
    ->add('title', null, ['label'=>'titre'])
```
Soit passer par le système de traduction :
- Dans __src/Form/AnnonceType.php__:
    ``` php
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Annonce::class,
            'translation_domain' => 'forms'
            // ici tu peux passer toutes les options que tu veux, on s'en servira plus tard pour afficher certains champ selon l'utilisateur connecté
        ]);
    }
    ```
- Changer la variable locale dans __config/services.yaml__ :
    ``` yaml
    parameters:
        locale: 'fr'
    ```
- Et ajoute le fichier suivant __translations/forms.fr.yaml__ :
    ``` yaml
    Title: Titre
    Description: Description
    Price: Prix
    Sold: Vendu
    Created at: Créé le
    Status: Status
    ```
Tes labels sont maintenant traduits !

## Les type de champ
Le champ __status__ serait plus adapté à un champ de type select. L'idée serait d'avoir un champ select avec les choix :
- Très mauvais ;
- Mauvais ;
- Bon ;
- Très bon ;
- Parfait.

### Exercice
En te référant à la [doc](https://symfony.com/doc/current/reference/forms/types.html), change les type de champs :
- __status__ en type _ChoiceType_ ;
- __createdAt__ en type _DateType_, afin que le champ se présente sous forme de datepicker et non plus en select ;
- tu peux aussi (ce n'est pas obligé) changer le champ description en y intégrant un _wysiwyg_ avec la librairie [Trumbowy](ghttps://alex-d.github.io/Trumbowyg/) par exemple.

---
---
---
![later](https://i.ytimg.com/vi/9jY4d6mGAUA/maxresdefault.jpg)

--- 
---
---

#### Correction
Dans __src/Form/AnnonceType.php__ :
``` php
# ne pas oublier
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
// ...
public function buildForm(FormBuilderInterface $builder, array $options)
{
    $builder
        //...
        ->add('status', ChoiceType::class, [
            'choices' => [
                'Très mauvais' => Annonce::STATUS_VERY_BAD,
                'Mauvais' => Annonce::STATUS_BAD,
                'Bon' => Annonce::STATUS_GOOD,
                'Très bon' => Annonce::STATUS_VERY_GOOD,
                'Parfait' => Annonce::STATUS_PERFECT
            ]
        ])
        ->add('createdAt',DateType::class, [
            'widget' => 'single_text', //tu peux lire https://symfony.com/doc/current/reference/forms/types/date.html#rendering-a-single-html5-text-box
        ])
    ;
}
```

## Création
Tu te souviens de la méthode ```new``` du contrôleur __src/Controller/AnnonceController.php__ ?
![mouhaha](https://media.giphy.com/media/3o6nV1ouOsNLBTEqQM/giphy.gif)

Maintenant que tu sais comment afficher un formulaire pour éditer une annonce, la création ne devrait pas poser de problème.

### Exercice
L'objectif dans cette méthode est d'afficher et d'enregistrer une nouvelle annonce via le formulaire.

Cette fois je ne mets pas les étapes. Je te laisse essayer, mais n'oublie pas te référer à la [doc](https://symfony.com/doc/current/forms.html) !

---
---
---
![later](https://i.ytimg.com/vi/LKSLjuMtmzQ/maxresdefault.jpg)

--- 
---
---



#### Correction

Dans __src/Controller/AnnonceController.php__ :
``` php
/**
 * @Route("/annonce/new")
 *
 * @return void
 */
public function new(Request $request, EntityManagerInterface $em)
{
    $annonce = new Annonce();

    $form = $this->createForm(AnnonceType::class, $annonce);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $em->persist($annonce);
        $em->flush();
        return $this->redirectToRoute('app_annonce_index');
    }
        
    return $this->render('annonce/new.html.twig', [
        'annonce' => $annonce,
        'form' => $form->createView()
    ]);
}
```
Le formulaire sera commun aux vues édition et création, tu peux donc faire un template commun. Crées le template __templates/annonce/_form.html.twig__ :
``` twig
{{ form_start(form) }}
    {{ form_widget(form) }}
    <button class="btn btn-primary">{{ button|default('Enregistrer') }}</button>
{{ form_end(form) }}
```

Crées un template __templates/annonce/new.html.twig__ :
``` twig
{% extends "_layout/sidebar.html.twig" %}
{% block title %}Nouvelle annonce!{% endblock %}
{% block content %}
    <h1>Nouvelle annonce</h1>
    {{ include("annonce/_form.html.twig", {form: form, button: 'Créer'}) }}
{% endblock %}
```

Tu peux aussi modifier le template __templates/annonce/edit.html.twig__ :
``` twig
{% extends "_layout/sidebar.html.twig" %}
{% block title %}Éditer l'annonce!{% endblock %}
{% block content %}
    <h1>Éditer l'annonce</h1>
    {{ include("annonce/_form.html.twig", {form: form, button: 'Éditer'}) }}
{% endblock %}
```

Ton formulaire de création est normalement fonctionnel !

## Suppression
Dans le controller __src/Controller/Admin/AnnonceController.php__, ajoute cette méthode:

``` php
/**
 * @Route("/annonce/{id}", methods="DELETE")
 */
public function delete(Annonce $annonce, EntityManagerInterface $em)
{
    // on supprime l'annonce de l'ObjetManager
    $em->remove($annonce);
    // en envoie la requête en base de données
    $em->flush();
    return $this->redirectToRoute('app_admin_annonce_index');
}
```
On peut remarquer qu'on demande à la route de ne répondre qu'à la méthode DELETE. En HTML, il n'est pas possible de spécifier cette méthode pour un formulaire, mais Symfony nous propose un petit "Hack".

Dans __templates/admin/annonce/index.html.twig__, ajoute ce petit formulaire:
``` php
<td>
    <a href="{{ path('app_admin_annonce_edit', {id: annonce.id}) }}" class="btn btn-secondary">Éditer</a>
    <form method="post" action="{{ path('app_admin_annonce_delete', {id: annonce.id}) }}">
        <input type="hidden" name="_method" value="DELETE">
        <button class="btn btn-danger">Supprimer</button>
    </form>
</td>
```
Tu peux remarquer l'input hidden et son ```name``` qui permet à Symfony de simuler l'envoie d'une requête DELETE.

Dans les versions supérieures à Symfony 4.4, pour que cela fonctionne, il faut aussi changer la valeur de ```http_method_override``` dans configuration de ```config/packages/framework.yaml```:
``` yaml
# see https://symfony.com/doc/current/reference/configuration/framework.html
framework:
    secret: '%env(APP_SECRET)%'
    http_method_override: true
```

Essaye de supprimer quelque chose en allant sur la route de suppression ! Tu devrait voir s'afficher __suppression__.

## Sécurité CSRF
Notre formulaire de suppression n'est pas sécurisé, une personne mal intentionnée peut tout à fait trouver et injecter ce formulaire sur le site et ainsi donner la possiblité à d'autres utilisateurs de supprimer des annonces sans même qu'ils s'en aperçoivent !

Nous allons donc utiliser un jeton [csrf](https://fr.wikipedia.org/wiki/Cross-site_request_forgery).

Si tu regardes tes autres formulaires, Symfony les a générés pour toi et il s'occupe de faire la vérification lui même. 

Ici, il s'agit d'un formulaire custom, c'est donc toi qui vas devoir générer et vérifier ce jeton.

Dans __templates/admin/annonce/index.html.twig__ :
``` twig
<form method="post" action="{{ path('app_admin_annonce_delete', {id: annonce.id}) }}">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token('delete' ~ annonce.id) }}">
    <button class="btn btn-danger">Supprimer</button>
</form>
```
Et dans __src/Controller/Admin/AnnonceController.php__ :
``` php
/**
 * @Route("/annonce/{id}", methods="DELETE")
 */
public function delete(Annonce $annonce, EntityManagerInterface $em, Request $request)
{
    if ($this->isCsrfTokenValid('delete' . $annonce->getId(), $request->get('_token'))) {
        $em->remove($annonce);
        $em->flush();
    }
    return $this->redirectToRoute('admin_annonce_index');
}
```
Tu peux vérifier que cela fonctionne en modifiant le jeton dans l'inspecteur de ton navigateur.

Tu peux aussi ajouter un message de validation avant suppression
``` twig
<form method="post" action="{{ path('app_admin_annonce_delete', {id: annonce.id}) }}" onsubmit="return confirm('Êtes vous vraiment sûr ?')">
```

## Message Flash
Les messages Flash vont te permettre d'afficher des messages en utilisant la session PHP.

### Exercice
Grâce à la [doc](https://symfony.com/doc/current/controller.html#flash-messages) (encore elle...), essaye d'afficher un message de succès si une annonce est bien supprimée.

#### Correction

---
---
---
![later](https://i.ytimg.com/vi/KUro66ItaBo/maxresdefault.jpg)

--- 
---
---

Dans __src/Controller/Admin/AnnonceController.php__ :
``` php
public function edit(Annonce $annonce, EntityManagerInterface $em, Request $request)
{
    $form = $this->createForm(AnnonceType::class, $annonce);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $em->flush();
        // ajout du message flash
        $this->addFlash('success', 'Annonce modifiée avec succès');
        return $this->redirectToRoute('app_admin_annonce_index');
    }
```
Et dans __templates/admin/annonce/index.html.twig__ :
``` twig
<h1>Gérer les annonces</h1>
{% for message in app.flashes('success') %}
    <div class="alert alert-success">
        {{ message }}
    </div>
{% endfor %}
```
---
You're done ! C'était pas facile. Bravo pour avoir fini cette partie !   
![Bravo](https://media.giphy.com/media/TgFibAJdezKXUYsddv/giphy.gif)
